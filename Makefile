# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
BINARY_NAME=pic2ascii

all: build

build:
	$(GOBUILD) -o $(BINARY_NAME) -v ./cmd/pic2ascii

clean: 
	$(GOCLEAN)
	rm -f $(BINARY_NAME)
