package main

import (
	"log"
	"os"
	"pic2ascii/p2a"
	"strings"
)

func main() {
	// Parse input options and flags
	inputFlags, err := p2a.ParseOptions()
	if err != nil {
		log.Fatal("ERROR: ", err)
	}

	// Read in the image
	log.Printf("Reading file %s\n", inputFlags.ImagePath)
	img, format, err := p2a.ReadImage(inputFlags.ImagePath)
	if err != nil {
		log.Fatal("ERROR: ", err)
	}

	// Create the reference to the correct output medium
	var f *os.File
	if inputFlags.OutputPath != "" {
		f, err = os.Create(inputFlags.OutputPath)
		if err != nil {
			log.Fatal("ERROR: ", err)
		}
		defer f.Close()
	} else {
		f = os.Stdout
	}

	// Process the image.
	log.Printf("Converting %s to ASCII\n", strings.ToUpper(format))
	if err := p2a.ProcessImage(img, f, inputFlags.Scale, inputFlags.Map); err != nil {
		log.Fatal("ERROR: ", err)
	}
	os.Exit(0)
}
