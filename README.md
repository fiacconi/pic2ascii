# pic2ascii

`pic2ascii` is a simple CLI program to transform an image into its ASCII representation.

`pic2ascii` is written in Go and it can handle images in JPG, PNG, and GIF.

`pic2ascii` transforms the input image in BW and then it maps the pixel values onto an ASCII "color map".
Two of those color masp are implemented, a finer one and a coarser one.

## Compiling `pic2ascii`

It is assumed that  a local installation of Go is available.

After cloning the repository, go into the repository directory; to compile the executable is just
```bash
make
```
The executable `pic2ascii` will appear in the repository directory.

## Using `pic2ascii`

As a CLI command, a simple help is available by doing
```bash
./pic2ascii --help
```
This will show all the available options.

To convert an image, it is as a simple as
```bash
./pic2ascii --image path/to/image
```
The ASCII version of the image is written on the stdout; to redirect the output to a new file, use
```bash
./pic2ascii --image path/to/image --output path/to/output/file
```

By default, `pic2ascii` makes an almost 1 to 1 mapping between pixels and ASCII characters (it corrects for typical font aspect ratio).
Therefore, the resulting ASCII representation may be too big.
To rescale the input image, use the `--scale` option.
For example, to reduce the input image size by a factor of 10, use
```bash
./pic2ascii --image path/to/image --scale 0.1
```

The starndard ASCII representation maps the 256 BW colours of 8-bit images onto 70 ASCII characters ordered by their "darkness".
However, a coarse but more consiste map of 10 ASCII characters is also available and it can be turned on with the `--fine-map` option, namely
```
./pic2ascii --image path/to/image --fine-map
```