package p2a

import (
	"flag"
	"fmt"
)

// Options is a struct that gathers the options that
// can be collected from CLI flags.
type Options struct {
	ImagePath  string
	OutputPath string
	Scale      float64
	Map        ASCIIMap
}

// ParseOptions is a public function to parse input options (expressed through CLI flags)
// and gathers all the results in a Options struct, returned together with an error.
func ParseOptions() (Options, error) {
	var ImagePath, OutputPath string
	var Scale float64
	var useLong bool

	flag.StringVar(&ImagePath, "image", "", "Path to the image to be processed.")
	flag.StringVar(&OutputPath, "output", "", "Where to write output. [DEFAULT=stdout]")
	flag.Float64Var(&Scale, "scale", 0.0, "Scaling factor to apply to the image. [DEFAULT=0, no scaling]")
	flag.BoolVar(&useLong, "fine-map", false, "Use long (i.e. finer) ASCII color map. [DEFAULT=false]")

	flag.Parse()

	if ImagePath == "" {
		return Options{}, fmt.Errorf("must specify input image")
	}

	var Map ASCIIMap
	if useLong {
		Map = LongMap
	} else {
		Map = ShortMap
	}

	return Options{ImagePath, OutputPath, Scale, Map}, nil
}
