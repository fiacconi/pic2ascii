package p2a

// ASCIIColor is the type to represent a gray shade in ASCII.
// Since the ASCII representation of the image is a matrix of ASCII characters, we use rune as a base type.
type ASCIIColor rune

// ASCIIColor implements the Stringer interface to simplify printing.
func (c ASCIIColor) String() string {
	return string(c)
}

// ASCIIMap is the base type to create an ASCII color map.
// It is a slice of ASCIIColor, i.e. ASCII characaters, ordered from the "darkest" to the "lightest" in terms of visual appearing.
type ASCIIMap []ASCIIColor

// Color2Ascii  is a method od ASCIIMap.
// It maps a 8bit (i.e. a uint8) grayscale color into the coresponding ASCII character from the ASCIIMap m.
func (m ASCIIMap) Color2Ascii(color uint8) ASCIIColor {
	scale := float64(color) / 256.0
	return m[int(float64(len(m))*scale)]
}

// ShortMap is a coarse-grained ASCII color map.
var ShortMap ASCIIMap = ASCIIMap{'@', '%', '#', '*', '+', '=', '-', ':', '.', ' '}

// LongMap is fine-grained ASCII color map.
var LongMap ASCIIMap = ASCIIMap{
	'$', '@', 'B', '%', '8', '&', 'W', 'M', '#', '*', 'o', 'a', 'h', 'k', 'b', 'd', 'p', 'q', 'w', 'm',
	'Z', 'O', '0', 'Q', 'L', 'C', 'J', 'U', 'Y', 'X', 'z', 'c', 'v', 'u', 'n', 'x', 'r', 'j', 'f', 't',
	'/', '\\', '|', '(', ')', '1', '{', '}', '[', ']', '?', '-', '_', '+', '~', '<', '>', 'i', '!', 'l',
	'I', ';', ':', ',', '"', '^', '`', '\'', '.', ' '}
