package p2a

import (
	"fmt"
	"image"
	_ "image/gif"  // To be able to decode GIF
	_ "image/jpeg" // To be able to decode JPEG
	_ "image/png"  // To be able to decode PNG
	"os"
	"strings"

	"github.com/nfnt/resize"
)

// ReadImage is public function to read an input image from its file path.
// It returns a decoded image.Image instance and an error.
func ReadImage(filepath string) (image.Image, string, error) {
	imageFile, err := os.Open(filepath)
	if err != nil {
		return nil, "", err
	}
	defer imageFile.Close()

	img, format, err := image.Decode(imageFile)
	if err != nil {
		return nil, "", err
	}
	return img, format, nil
}

// resizeImage is a private function to resize an image according to a scale.
// Moreover, the image is further shrinked on the vertical direction to account
// for the aspect ratio of fonts that are typically about a factor 2 taller than wider.
func resizeImage(img image.Image, scale float64) image.Image {
	w, h := img.Bounds().Size().X, img.Bounds().Size().Y
	h = int(float32(h) * 0.5)
	if scale > 0 {
		w, h = int(float64(w)*scale), int(float64(h)*scale)
	}
	return resize.Resize(uint(w), uint(h), img, resize.Bilinear)
}

// makeImageBW is a private function to transform an image from RGBA to BW.
func makeImageBW(img image.Image) *image.Gray {
	grayImage := image.NewGray(img.Bounds())
	for y := img.Bounds().Min.Y; y < img.Bounds().Max.Y; y++ {
		for x := img.Bounds().Min.X; x < img.Bounds().Max.X; x++ {
			grayImage.Set(x, y, img.At(x, y))
		}
	}
	return grayImage
}

// ProcessImage is the master function that takes an image.Image, it rescales it if required,
// it converts it to BW and then maps the pixel colors onto ASCII characters according to
// the ASCIIMap provided. It finally prints the ACSII-translated image on the provided support.
func ProcessImage(img image.Image, f *os.File, scale float64, asciimap ASCIIMap) error {

	imgScaled := resizeImage(img, scale)

	imgBW := makeImageBW(imgScaled)

	for y := imgBW.Bounds().Min.Y; y < imgBW.Bounds().Max.Y; y++ {

		var str strings.Builder
		str.Grow(imgBW.Bounds().Size().X)

		for x := imgBW.Bounds().Min.X; x < imgBW.Bounds().Max.X; x++ {
			str.WriteRune(rune(asciimap.Color2Ascii(imgBW.GrayAt(x, y).Y)))
		}

		n, err := fmt.Fprintln(f, str.String())
		if n == 0 || err != nil {
			return err
		}
	}
	return nil
}
